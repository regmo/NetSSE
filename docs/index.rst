Welcome to NetSSE's documentation!
==================================

.. toctree::
   :maxdepth: 2
   :hidden:

   User Guide <user_guide/index>
   Examples <examples/index>
   API Reference <api/index>
   Release Notes <releases/index>
   About <project/index>


What is NetSSE?
---------------

**NetSSE** is an open-source Python package for network-based sea state estimation from ships, buoys, and other observation platforms. The objective of NetSSE is to provide a scientific framework to develop, test, and distribute new sea state estimation methods. All code is available at the `NetSSE GitLab <https://gitlab.gbar.dtu.dk/regmo/NetSSE>`_.


.. note::

   This project is under active development and will be running until December 2027.


.. grid::

    .. grid-item-card:: :fas:`user;pst-color-primary` User Guide
        :link: user_guide/index
        :link-type: doc

        User guide on installation and the basic concepts of NetSSE.

    .. grid-item-card:: :fas:`book-open-reader;pst-color-primary` Examples
        :link: examples/index
        :link-type: doc

        Examples of NetSSE usage.

    .. grid-item-card:: :fas:`laptop-code;pst-color-primary` API Reference
        :link: api/index
        :link-type: doc

        NetSSE application programming interface (API) reference.


.. grid::

    .. grid-item-card:: :fas:`book-bookmark;pst-color-primary` Publications
        :link: project/publications
        :link-type: doc

        Find an overview of scientific peer-reviewed studies that used NetSSE.

    .. grid-item-card:: :fas:`envelope;pst-color-primary` Contact us
        :link: project/contact
        :link-type: doc

        Want to contribute to the project? Get to know the developer team and reach out to us.


Quick Example
-------------

.. todo::
    This section is still under development.


Using NetSSE? Please cite us!
-----------------------------

If you find NetSSE useful and use it in your research or project, we kindly ask you to credit the developers using below reference:

.. code-block:: text

    Mounet, R. E. G., & Nielsen, U. D. NetSSE: An open-source Python package for network-based sea state estimation from ships, buoys, and other observation platforms (version 2.0). Technical University of Denmark, GitLab. July 2024. https://doi.org/10.11583/DTU.26379811.


Documentation in .PDF format
----------------------------

A .PDF version of the latest NetSSE documentation is available for download `here <https://netsse.readthedocs.io/_/downloads/en/latest/pdf/>`_. As a disclaimer, the .PDF document is automatically generated and may therefore contain errors or inconsistencies. To avoid any confusion, the `latest online documentation <https://netsse.readthedocs.io/en/latest/index.html>`_ should always be consulted, if possible.