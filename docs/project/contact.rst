Contact
=======

For any enquiry about the software, please contact the main developer 
`Raphaël E. G. Mounet <https://orbit.dtu.dk/en/persons/rapha%C3%ABl-emile-gilbert-mounet>`_.


Mailing list
------------

NetSSE has a newsletter through which occasional updates are sent when new functionalities are 
added to the package. The mailing list receives up to 1-3 emails yearly.

If you wish to subscribe to the newsletter, please fill in this sign-up form:

.. grid:: auto

    .. grid-item::

        .. button-link:: http://eepurl.com/iSaEus
            :ref-type: ref
            :color: info
            :shadow:

            Sign-up 