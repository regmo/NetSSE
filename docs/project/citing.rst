Citing and logo
===============

If you find NetSSE useful and use it in your research or project, we kindly ask you to credit the developers using below reference:

.. code-block:: text

    Mounet, R. E. G., & Nielsen, U. D. NetSSE: An open-source Python package for network-based sea state estimation from ships, buoys, and other observation platforms (version 2.0). Technical University of Denmark, GitLab. July 2024. https://doi.org/10.11583/DTU.26379811.


BibTeX reference
----------------

.. code-block:: text

    @software{NetSSE2024,
      author        = {Raphaël E. G. Mounet and Ulrik D. Nielsen},
      title         = {{NetSSE: An open-source Python package for network-based sea state 
                        estimation from ships, buoys, and other observation platforms}},
      month         = {8},
      year          = {2024},
      publisher     = {Technical University of Denmark, GitLab},
      version       = {version 2.0},
      url           = {https://gitlab.gbar.dtu.dk/regmo/NetSSE},
      doi           = {https://doi.org/10.11583/DTU.26379811},
    }


Logo
----

The current logo of NetSSE was designed by `Jennifer Mounet <https://www.linkedin.com/in/jennifer-mounet-749849182/?locale=en_US>`_ in 2024.


.. figure:: ../_static/NetSSE_2_logo_bare-03.png
   :figwidth: 10em
   :align: center
   :alt: Dark logo of NetSSE version 2.0
   :target: ../_static/NetSSE_2_logo_bare-03.png
   :class: only-light
.. figure:: ../_static/NetSSE_2_logo_bare-04.png
   :figwidth: 10em
   :align: center
   :alt: Light logo of NetSSE version 2.0
   :target: ../_static/NetSSE_2_logo_bare-04.png
   :class: only-dark


Colour
------

This subsection is under development.

..
    .. raw:: html

        <table class="table">
            <tr>
                <td style="text-align: center;">
                    <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100">
                        <circle cx="50" cy="50" r="50" fill="#009BE0"/>
                    </svg>
                    <br/>
                    <b style="color: #009BE0;">NetSSE Blue</b><br/>
                    RGB: R0 G155 B224<br/>
                    HEX: #009BE0
                </td>
            </tr>
        </table>