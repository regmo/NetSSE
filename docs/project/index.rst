About the project
=================

.. only:: html
   
   .. figure:: ../_static/DTU_Red.svg
      :figwidth: 5em
      :align: left
   

**NetSSE** is the result of ongoing work carried out from 2020 onwards by researchers at the `Technical University of Denmark (DTU) <https://www.dtu.dk/english/>`_, `Department of Civil and Mechanical Engineering <https://construct.dtu.dk/>`_, `Section of Fluid Mechanics, Coastal and Maritime Engineering <https://construct.dtu.dk/sections/fluid-mechanics-coastal-and-maritime-engineering>`_.


Motivation
----------

Ocean waves represent the main source of dynamic excitation of surface vessels and floating marine structures. The scarcity of wave data from vast areas of the world's oceans is yet an ongoing problem. The crucial quantification and mitigation of uncertainties inherent to wave monitoring is increasingly recognised by the shipping, offshore, and renewable energy industries, as well as in coastal engineering [#REGMOPhDhesis]_.

Fusing data from multiple and heterogeneous observation platforms enables reducing the uncertainties of measurements from individual platforms. In this framework, the **ship-as-a-wave-buoy** concept shows high relevance, simply by accounting for the sheer number of ships in transit on the open sea. The wave-induced responses of modern vessels are often monitored by inexpensive off-the-shelf sensors, therefore constituting data that can be used for estimating sea states in near real-time, in a fundamentally similar way to traditional wave-rider buoys. **Vessel response-based estimates of the sea state**, combined with measurements from other met-ocean sensors, can be integrated into observation networks to improve the reliability and availability of wave data [#REGMOPhDhesis]_.

With this in mind, **NetSSE** -- as the contraction of "Network" and "Sea State Estimation" -- can be seen as a toobox to help scientists and engineers experiment with networks of wave sensor systems, especially integrating **vessels as “sailing wave buoys”**. NetSSE can be used for instance to generate and process synthetic data of vessel responses, to extract sample results from model-scale experiments, and to analyse and merge together real-world data collected by seagoing vessels and other observation platforms.

The project's overall aim is to improve the sampling and modelling of the ocean wave environment around the globe, thereby contributing to scientific understanding, as well as to the sustainability and operational safety of ocean-based human activities. This shall positively contribute to the Sustainable Development Goals (SDGs) postulated by the United Nations, having the main effects aligned with SDG 7 (“Affordable and clean energy”), SDG 13 (“Climate action”), SDG 14 (“Life below water”), and SDG 17 (“Partnerships for the goals”).


Funding acknowledgment
----------------------

2024-now
********

The main developer's postdoctoral work is financially supported by the `Independent Research Fund of Denmark <https://dff.dk/en>`_ (grant ID: 10.46540/3164-00087B) and `Orients Fond <https://orientsfond.dk/>`_ (case: SEAFUSION under the framework agreement ‘Orients Fond 2021-2025’).


2020-2023
*********

The main developer's doctoral work was partly supported by the `Research Council of Norway <https://www.forskningsradet.no/en/>`_ through the Centres of Excellence funding scheme [project number 223254, NTNU AMOS, Center for Autonomous Marine Operations and Systems]. Moreover, the financial support from the `Danish Maritime Fund <https://dendanskemaritimefond.dk/english/>`_ received in 2017 and 2020 is greatly acknowledged.


Contents
--------

.. toctree::

   team
   citing
   license
   publications
   contact

.. rubric:: References

.. [#REGMOPhDhesis] Mounet, R. E. G. *Sea state estimation based on measurements from multiple observation 
    platforms*. Ph.D. dissertation, Technical University of Denmark, 2023.
