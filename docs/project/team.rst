The NetSSE team
===============

.. grid:: auto

    .. grid-item::
        
        .. only:: html

            .. image:: https://orbit.dtu.dk/files-asset/340107783/134729_423ae243.jpg?w=160&f=webp
                :width: 11em
                :align: center
                :alt: REGMO's picture
                :target: https://orbit.dtu.dk/en/persons/rapha%C3%ABl-emile-gilbert-mounet

            Raphaël E. G. Mounet (*DTU Construct*)

        .. only:: latex
            
            `Raphaël E. G. Mounet <https://orbit.dtu.dk/en/persons/rapha%C3%ABl-emile-gilbert-mounet>`_, *DTU Construct*


    .. grid-item::

        .. only:: html

            .. image:: https://orbit.dtu.dk/files-asset/340097418/12409_0c1d2a77.jpg?w=160&f=webp
                :width: 11em
                :align: center
                :alt: UDNI's picture
                :target: https://orbit.dtu.dk/en/persons/ulrik-dam-nielsen
            
            Ulrik D. Nielsen (*DTU Construct*)

        .. only:: latex

            `Ulrik D. Nielsen <https://orbit.dtu.dk/en/persons/ulrik-dam-nielsen>`_, *DTU Construct*
        