Publications
============

The following research publications have used the NetSSE package.


Journal papers
--------------

1. Mounet, R.E.G., Chen, J., Nielsen, U.D., Brodtkorb, A.H., Pillai, A.C., Ashton, I.G.C., Steele, E.C.C. (2023). 
   **Deriving Spatial Wave Data from a Network of Buoys and Ships**. Ocean Engineering, 281, 114892. 
   https://doi.org/10.1016/j.oceaneng.2023.114892.

2. Mounet, R.E.G., Nielsen, U.D., Brodtkorb, A.H., Øveraas, H., Dallolio, A., Johansen, T.A. (2024). 
   **Data-Driven Method for Hydrodynamic Model Estimation Applied to an Unmanned Surface Vehicle**. 
   Measurement, 234, 114724. https://doi.org/10.1016/j.measurement.2024.114724.


Conference papers
-----------------

3. Mounet, R.E.G., Nielsen, U.D., Brodtkorb, A.H. (2023). 
   **Doppler Shift Approximation for Predicting the Wave-Induced Response of Advancing Vessels in Following Waves**. 
   Proceedings of the ASME 2023 42nd International Conference on Ocean, Offshore and Arctic Engineering (OMAE’23). 
   https://doi.org/10.1115/OMAE2023-107733.

4. Mounet, R.E.G., Nielsen, U.D. (2024). **An Application of the Extended Maximum Entropy
   Principle for the Spectral Analysis of Ship Motions**. Proceedings of the 2024 IEEE International Workshop on 
   Metrology for the Sea. (*Under review*).


Other publications
------------------

5. Mounet, R.E.G. (2023). **Sea State Estimation Based on Measurements from Multiple Observation Platforms**. 
   PhD Thesis. Technical University of Denmark. 
   https://orbit.dtu.dk/en/publications/sea-state-estimation-based-on-measurements-from-multiple-observat

