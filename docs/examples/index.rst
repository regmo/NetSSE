.. _examples:

Examples
========

Below, you can find examples of how NetSSE functions are used for sea state estimation from
various observation platforms.

Examples in the form of Python scripts can also be found in the `examples directory on GitLab
<https://gitlab.gbar.dtu.dk/regmo/NetSSE/-/tree/master/docs/examples>`_.


.. todo::
    This section is still under development.


Contents
--------

.. toctree::
   :maxdepth: 1
   :numbered:

   example1
   example_network