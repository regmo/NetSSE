Glossary
========

.. glossary::

    AI
        Artifical Intelligence.

    ANN
        Artificial Neural Network.

    CFD
        Computational Fluid Dynamics.

    CFE
        Closed-Form Expression.

    DoF
        Degree of Freedom.

    DP  
        Dynamic Positioning.

    DSF
        Directional Spreading Function.

    DTU
        Danmarks Tekniske Universitet / Technical University of Denmark.

    DWS
	    Directional Wave Spectrum.

    EMEP
        Extended Maximum Entropy Principle.

    FFT
        Fast Fourier Transform.

    IMU
        Inertial Measurement Unit.

    JONSWAP
        Joint North Sea Wave Project

    FIFO
        First-In/First-Out.

    ML
        Machine Learning.

    MSL
        Mean Sea Level.

    NED
        North-East-Down.

    PSD
        Power Spectral Density.

    RAO
        Response Amplitude Operator.

    SAWB
        Ship-As-a-Wave-Buoy

    SSE
        Sea State Estimation.
        
        An ensemble of methods and techniques used to characterise the ocean wave system at a given time and position.

    std.
        Standard deviation.

    USV
        Unmanned Surface Vehicle.

    WBA
        Wave Buoy Analogy