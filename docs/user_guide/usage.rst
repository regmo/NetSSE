Usage
=====

Summary of present functionalities
----------------------------------

**NetSSE consists of a Python implementation that**:
   
- Proposes a structure for the establishment of a functional, heterogeneous network of wave sensor systems, including vessels as “sailing wave buoys”;
- Facilitates the data-driven estimation of vessel hydrodynamic models to mathematically describe the response dynamics of floating observation platforms in waves;
- Enables the fusion of on-site spectral wave data from various kinds of in-situ observation platforms located in the same sea state;
- Allow the spatial estimation of wave conditions within a large geographical domain, integrating individual sea state estimates derived from the records of ship responses.


Outlook
-------

The developer team hopes that the current and future versions of NetSSE be relevant, accessible, and beneficial to a large range of stakeholders, not only from the various industries with interests in the sea but, equally important, from the international research community in ocean engineering and science, as well as international institutions and local governmental agencies.