Release notes
=============

This is the list of changes to NetSSE between each release. For full details, see the `commit logs <https://gitlab.gbar.dtu.dk/regmo/NetSSE/-/commits/>`_ on GitLab. For install and upgrade instructions, see :ref:`installation`.

.. _1.0: https://gitlab.gbar.dtu.dk/regmo/NetSSE/-/releases/NetSSE_1.0

.. _2.0: https://gitlab.gbar.dtu.dk/regmo/NetSSE/-/releases/NetSSE_2.0.0

+------------+--------------+-------------------------------------+
| Version    | Release date | New features                        | 
+============+==============+=====================================+
| `1.0`_     | 27-07-2023   | *Initial modules*:                  |
|            |              | analys.buoy, analys.spec,           |
|            |              | model.ship,                         |
|            |              | simul.ship_resp,                    |
|            |              | tools.envir_cond, tools.misc_func.  |
+------------+--------------+-------------------------------------+
| `2.0`_     | 26-07-2024   | *New modules*:                      |
| (latest)   |              | base,                               |
|            |              | analys.emep, analys.enc_spec,       |
|            |              | analys.sawb.                        |   
+------------+--------------+-------------------------------------+