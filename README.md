<img src="docs/_static/NetSSE_2_logo_bare-01.png" width="100">

# NetSSE, version 2.0

_Release date: 26-07-2024._

This is the NetSSE open-source Python package for network-based sea state estimation from ships, buoys, and other observation platforms.

NetSSE is the result of ongoing work carried out from 2020 onwards by researchers at the Technical University of Denmark (DTU), Department of Civil and Mechanical Engineering, Section of Fluid Mechanics, Coastal and Maritime Engineering.


### Documentation

The full documentation, installation guide, and examples are provided at [this page](https://netsse.readthedocs.io/).


### How to credit this work?

If you find NetSSE useful and use it in your research or project, we kindly ask you to credit the developers using below reference:

	Mounet, R. E. G., & Nielsen, U. D. NetSSE: An open-source Python package for network-based sea state estimation from ships, buoys, and other observation platforms (version 2.0). Technical University of Denmark, GitLab. July 2024. https://doi.org/10.11583/DTU.26379811.


### Contact

For any enquiry about the software, please contact the main developer, [Raphaël E. G. Mounet](https://orbit.dtu.dk/en/persons/rapha%C3%ABl-emile-gilbert-mounet).


### Mailing list

If you wish to subscribe to the NetSSE mailing list, please fill in this [signup form](http://eepurl.com/iSaEus).

You will then receive occasional updates when new functionalities are added to NetSSE. You can expect 1-3 emails yearly.


### Acknowledgements

The main developer’s postdoctoral work is financially supported by the Independent Research Fund of Denmark (grant ID: 10.46540/3164-00087B) and Orients Fond (case: SEAFUSION under the framework agreement ‘Orients Fond 2021-2025’).


### License

NetSSE is distributed under the GNU General Public License version 3. This program comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under certain conditions; for details, see the [license terms](/LICENCE.md).

Copyright © 2024 Technical University of Denmark, Raphaël E. G. Mounet.